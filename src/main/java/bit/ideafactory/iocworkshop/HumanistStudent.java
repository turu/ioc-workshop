package bit.ideafactory.iocworkshop;

import bit.ideafactory.iocworkshop.studentkit.IEraser;
import bit.ideafactory.iocworkshop.studentkit.IPaper;
import bit.ideafactory.iocworkshop.studentkit.IWriter;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * @author Michal Partyka
 */
public class HumanistStudent extends AbstractStudent {
    private IEraser eraser;
    private IWriter writer;

    @Override
    public void writeEssay(List<String> paragraphs) {
        for (String paragraph : paragraphs) {
            writeParagraph(paragraph);
        }
    }

    private String fixParagraph(String paragraph) {
        return eraser.eraseLast(paragraph);
    }

    private void writeParagraph(String paragraph) {
        final String writtenParagraph = writer.write(paragraph);
        getPaper().addParagraph(writtenParagraph);
        final String fixedParagraph = fixParagraph(writtenParagraph);
        getPaper().updateLastParagraph(fixedParagraph);
    }

    public HumanistStudent(IPaper paper) {
        super(paper);
    }

    @Required
    public void setWriter(IWriter writer) {
        this.writer = writer;
    }

    public IWriter getWriter() {
        return writer;
    }

    @Required
    public void setEraser(IEraser eraser) {
        this.eraser = eraser;
    }

    public IEraser getEraser() {
        return eraser;
    }
}
