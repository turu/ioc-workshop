package bit.ideafactory.iocworkshop;

import bit.ideafactory.iocworkshop.studentkit.IPaper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Michal Partyka
 */
public class StudentClass {
    private final List<IStudent> students = new ArrayList<IStudent>();

    public StudentClass(List<IStudent> students) {
        this.students.addAll(students);
    }

    public Map<IStudent, IPaper> writeEssay(List<String> paragraphs) {
        Map<IStudent, IPaper> result = new HashMap<IStudent, IPaper>();

        for(IStudent student : students) {
            student.writeEssay(paragraphs);
            result.put(student, student.getPaper());
        }

        return result;

    }


}
