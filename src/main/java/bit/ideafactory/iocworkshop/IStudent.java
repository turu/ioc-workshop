package bit.ideafactory.iocworkshop;

import bit.ideafactory.iocworkshop.studentkit.IPaper;

import java.util.List;

/**
 * @author Michal Partyka
 */
public interface IStudent {
    void writeEssay(List<String> paragraphs);

    IPaper getPaper();
}
