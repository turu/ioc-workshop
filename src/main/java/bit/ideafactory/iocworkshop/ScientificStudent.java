package bit.ideafactory.iocworkshop;

import bit.ideafactory.iocworkshop.studentkit.IPaper;
import bit.ideafactory.iocworkshop.studentkit.IWriter;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * @author Michal Partyka
 */
public class ScientificStudent extends AbstractStudent {
    private IWriter writer;

    @Override
    public void writeEssay(List<String> paragraphs) {
        for (String paragraph : paragraphs) {
            String write = writer.write(paragraph);
            getPaper().addParagraph(write);
        }
    }

    public ScientificStudent(IPaper paper) {
        super(paper);
    }

    @Required
    public void setWriter(IWriter writer) {
        this.writer = writer;
    }

    public IWriter getWriter() {
        return writer;
    }
}
