package bit.ideafactory.iocworkshop.studentkit;

/**
 * @author Michal Partyka
 */
public interface IEraser {
    String eraseLast(String message);
}
