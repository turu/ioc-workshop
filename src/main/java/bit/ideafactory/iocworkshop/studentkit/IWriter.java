package bit.ideafactory.iocworkshop.studentkit;

/**
 * @author Michal Partyka
 */
public interface IWriter {
    String write(String message);
}
