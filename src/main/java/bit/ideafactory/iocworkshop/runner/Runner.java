package bit.ideafactory.iocworkshop.runner;

import bit.ideafactory.iocworkshop.IStudent;
import bit.ideafactory.iocworkshop.StudentClass;
import bit.ideafactory.iocworkshop.studentkit.IPaper;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Michal Partyka
 */
public class Runner {

    private final ClassPathXmlApplicationContext context;

    private StudentClass klass;

    public Runner() {
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }

    public static void main(String[] args) {
        final Runner runner = new Runner();
        runner.init();
        runner.run();
    }

    private void run() {
        //given
        List<String> paragraphs = Arrays.asList("par1", "par2", "par3");

        //when
        Map<IStudent,IPaper> result = klass.writeEssay(paragraphs);

        logResults(result);
    }

    private void logResults(Map<IStudent, IPaper> result) {
        System.out.println("Printing results of essay." + result.size() + " students participated");
        for (Map.Entry<IStudent, IPaper> entry : result.entrySet()) {
            System.out.println("Student: " + entry.getKey() + " came up with: " + entry.getValue().toString());
        }
    }

    private void init() {

        klass = (StudentClass) context.getBean("studentClass");
    }
}
