package bit.ideafactory.iocworkshop;

import bit.ideafactory.iocworkshop.studentkit.IEraser;
import bit.ideafactory.iocworkshop.studentkit.IPaper;
import bit.ideafactory.iocworkshop.studentkit.IWriter;
import bit.ideafactory.iocworkshop.studentkit.erasers.Rubber;
import bit.ideafactory.iocworkshop.studentkit.paper.LessonPaper;
import bit.ideafactory.iocworkshop.studentkit.writers.Pencil;

import java.util.List;

/**
 * @author Michal Partyka
 */
public class Student implements IStudent {
    private final IEraser rubber;
    private final IWriter pencil;
    private final IPaper paper;

    public Student(IEraser rubber, IWriter pencil, IPaper paper) {
        this.rubber = rubber;
        this.pencil = pencil;
        this.paper = paper;
    }

    public Student() {
        paper = new LessonPaper();
        rubber = new Rubber();
        pencil = new Pencil();
    }

    @Override
    public void writeEssay(List<String> paragraphs) {
        for (String paragraph : paragraphs) {
            writeParagraph(paragraph);
        }
    }

    @Override
    public IPaper getPaper() {
        return paper;
    }

    private String fixParagraph(String paragraph) {
        return rubber.eraseLast(paragraph);
    }

    private void writeParagraph(String paragraph) {
        final String writtenParagraph = pencil.write(paragraph);
        paper.addParagraph(writtenParagraph);
        final String fixedParagraph = fixParagraph(writtenParagraph);
        paper.updateLastParagraph(fixedParagraph);
    }


}
