package bit.ideafactory.iocworkshop;

import bit.ideafactory.iocworkshop.studentkit.IPaper;
import bit.ideafactory.iocworkshop.studentkit.IWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Michal Partyka
 */
public class FoolStudent extends AbstractStudent {
    private IWriter writer;

    @Override
    public void writeEssay(List<String> paragraphs) {
        StringBuilder builder = new StringBuilder();

        for (String par : paragraphs) {
            builder.append(par);
        }
        String write = writer.write(builder.toString());
        getPaper().addParagraph(write);

    }

    public FoolStudent(IPaper paper) {
        super(paper);
    }

    public void setWriter(IWriter writer) {
        this.writer = writer;
    }

    public IWriter getWriter() {
        return writer;
    }
}
