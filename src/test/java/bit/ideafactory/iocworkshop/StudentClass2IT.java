package bit.ideafactory.iocworkshop;

import bit.ideafactory.iocworkshop.studentkit.IPaper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;


/**
 * @author Michal Partyka
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class StudentClass2IT {

    @Autowired
    @Qualifier(value = "studentClass")
    private StudentClass studentClass;

    @Test
    public void testClass() throws Exception {
        List<String> par = Arrays.asList("par1", "par2", "par3");

        Map<IStudent,IPaper> studentPaperMap = studentClass.writeEssay(par);

        Collection<IPaper> values = studentPaperMap.values();
        assertThat(values).hasSize(3);
    }
}
