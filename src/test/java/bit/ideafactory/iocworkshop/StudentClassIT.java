package bit.ideafactory.iocworkshop;

import bit.ideafactory.iocworkshop.studentkit.IEraser;
import bit.ideafactory.iocworkshop.studentkit.IPaper;
import bit.ideafactory.iocworkshop.studentkit.IWriter;
import bit.ideafactory.iocworkshop.studentkit.erasers.Rubber;
import bit.ideafactory.iocworkshop.studentkit.writers.Pen;
import bit.ideafactory.iocworkshop.studentkit.writers.Pencil;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Michal Partyka
 */
public class StudentClassIT {

    private StudentClass klass;

    private IStudent scientificStudent;
    private IStudent humanistStudent;
    private IStudent foolStudent;


    private IWriter pen = new Pen();
    private IWriter pencil = new Pencil();
    private IEraser rubber = new Rubber();

    @Before
    public void setUp() throws Exception {
//        foolStudentFactory = new FoolStudentFactory(examPaperFactory, pen);
//        humanistStudentFactory = new HumanistStudentFactory(lessonPaperFactory, pencil, rubber);
//        scientificStudentFactory = new ScientificStudentFactory(examPaperFactory, pen);

//        foolStudent = foolStudentFactory.create();
//        scientificStudent = scientificStudentFactory.create();
//        humanistStudent = humanistStudentFactory.create();

        klass = new StudentClass( Arrays.asList(scientificStudent, foolStudent, humanistStudent) );
    }

    @Test
    public void writeEssay_should() throws Exception {
        //given
        List<String> paragraphs = Arrays.asList("par1", "par2", "par3");

        //when
        Map<IStudent,IPaper> result = klass.writeEssay(paragraphs);

        //then
        assertThat(result.get(foolStudent).toString()).isEqualTo("*par1par2par3*");
    }
}
