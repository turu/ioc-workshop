package bit.ideafactory.iocworkshop;

import bit.ideafactory.iocworkshop.studentkit.IPaper;
import bit.ideafactory.iocworkshop.studentkit.erasers.Rubber;
import bit.ideafactory.iocworkshop.studentkit.paper.LessonPaper;
import bit.ideafactory.iocworkshop.studentkit.writers.Pencil;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Author: Piotr Turek
 */
public class StudentIT {
    private IStudent student;

    @Before
    public void setUp() throws Exception {
        student = new Student(new Rubber(), new Pencil(), new LessonPaper());
    }


    @Test
    public void testWriteEssay() throws Exception {
        //given
        final List<String> pars = Arrays.asList("Par1", "Par2", "Par3");

        //when
        student.writeEssay(pars);

        //then
        final IPaper paper = student.getPaper();
        assertThat(paper.toString()).isEqualTo("^Par^^Par^^Par^");
    }

}
